
public class Benutzer {

	private	String name;
	private	String passwort;
	private	String email;
	private String berechtigungsstatus;
	
		
	public Benutzer(String name) {			//Konstruktor
		this.name = name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public void setPW(String passwort) {
		this.passwort = passwort;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setBerechtigungsstatus(String berechtigungsstatus) {
		this.berechtigungsstatus = berechtigungsstatus;
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPW() {
		return this.passwort;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getBerechtigungsstatus() {
		return this.berechtigungsstatus;
	}
	
	
	
}
