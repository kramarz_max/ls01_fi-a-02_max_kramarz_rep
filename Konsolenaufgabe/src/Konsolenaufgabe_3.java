
public class Konsolenaufgabe_3 {
	
	public static void main(String[] args) {
		System.out.printf("%-12s|%10s\n",  "Fahrenheit","Celsius");
		System.out.printf("%-12s\n", "------------------------");
		System.out.printf("%-12s|%10.2f\n",  "-20", -28.889);
		System.out.printf("%-12s|%10.2f\n",  "-10", -23.333);
		System.out.printf("%-12s|%10.2f\n",  "+0", -17.788);
		System.out.printf("%-12s|%10.2f\n",  "+10", -6.6777);
		System.out.printf("%-12s|%10.2f",  "+20", -1.1111);
	}
}