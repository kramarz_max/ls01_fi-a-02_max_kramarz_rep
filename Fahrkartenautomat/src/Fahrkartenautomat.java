﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {

		char weiter = 'j';

		while (weiter == 'j') {

			Scanner tastatur = new Scanner(System.in);

			double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

			double geldeinwurf = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

			fahrkartenAusgeben();

			rueckgeldAusgeben(zuZahlenderBetrag, geldeinwurf);

			System.out.println("Möchten Sie eine weitere Karte kaufen? [j/n]?");
			weiter = tastatur.next().charAt(0);

		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	private static void fahrkartenAusgeben() {
		// TODO Auto-generated method stub
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	private static void rueckgeldAusgeben(double zuZahlenderBetrag, double geldeinwurf) {
		// TODO Auto-generated method stub
		double rückgabebetrag;

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rückgabebetrag = geldeinwurf - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s\n", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = rückgabebetrag * 10;
				Math.round(rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 10;

			}

		}

	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f%s\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;

	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		int fahrscheinEingabe;
		int auswahlnummer;

		String[] fahrkartenBezeichnung = new String[10];

		fahrkartenBezeichnung[0] = "Einzelfahrschein Berlin AB";
		fahrkartenBezeichnung[1] = "Einzelfahrschein Berlin BC";
		fahrkartenBezeichnung[2] = "Einzelfahrschein Berlin ABC";
		fahrkartenBezeichnung[3] = "Kurzstrecke";
		fahrkartenBezeichnung[4] = "Tageskarte Berlin AB";
		fahrkartenBezeichnung[5] = "Tageskarte Berlin BC";
		fahrkartenBezeichnung[6] = "Tageskarte Berlin ABC";
		fahrkartenBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";

		double[] fahrkartenPreis = new double[10];

		fahrkartenPreis[0] = 2.90;
		fahrkartenPreis[1] = 3.30;
		fahrkartenPreis[2] = 3.60;
		fahrkartenPreis[3] = 1.90;
		fahrkartenPreis[4] = 8.60;
		fahrkartenPreis[5] = 9.00;
		fahrkartenPreis[6] = 9.60;
		fahrkartenPreis[7] = 23.50;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;

		System.out.println("Wählen Sie Ihre Wunschfahrkarte aus: ");
		for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
			int auswahl = i + 1;
			System.out.printf("%s%.2f%s\n", fahrkartenBezeichnung[i] + " [", +fahrkartenPreis[i],
					"EUR] (" + auswahl + ")");
		}

		System.out.print("\nIhre Wahl: ");

		auswahlnummer = tastatur.nextInt();

		while (auswahlnummer < 0 || auswahlnummer > 10) {
			System.out.println("Fehler! ungültige Auswahlnummer.");
			System.out.print("\nIhre Wahl: ");
			auswahlnummer = tastatur.nextInt();
		}

		double fahrkartenAuswahlPreis = fahrkartenPreis[auswahlnummer - 1];

		System.out.print("\nAnzahl der Tickets: ");
		fahrscheinEingabe = tastatur.nextInt();

		if (!(fahrscheinEingabe >= 1 && fahrscheinEingabe <= 10)) {
			fahrscheinEingabe = 1;
			System.out.println("Anzahl der Karten ungültig. Es wird mit einer Karte fortgefahren.");
		}

		return fahrkartenAuswahlPreis * fahrscheinEingabe;

	}
}
