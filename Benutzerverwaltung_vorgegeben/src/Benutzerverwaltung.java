import java.io.*;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        try {
				BenutzerverwaltungV10.start();
						
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
        }
    }
}



class BenutzerverwaltungV10{
    public static void start() throws FileNotFoundException, IOException, ClassNotFoundException{
        Scanner tastatur = new Scanner(System.in);
        
      /*  
        benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        
       
        ObjectOutputStream benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();
       
       */
        ObjectInputStream benutzerdatei = new ObjectInputStream(new FileInputStream("Benutzer.bin"));
        BenutzerListe benutzerListe = (BenutzerListe)benutzerdatei.readObject();
        benutzerdatei.close();

        System.out.println(benutzerListe.select());

        while (true) {
                	
        	
        	

            System.out.println("Was moechten Sie tun?\n");
            System.out.println("Anmelden (1)");
            System.out.println("Registrieren (2)");
            System.out.println("Löschen (3)");
            System.out.println("Herunterfahren (4)\n");
            
            System.out.print("Ihre Auswahl: ");
	       
            int auswahl = tastatur.nextInt();
            
            if (auswahl == 1) {
	        	anmelden(tastatur, benutzerListe);
	        } else if (auswahl == 2) {
	        	registrieren(tastatur, benutzerListe);
	        } else if (auswahl ==3 ) {
	        	loeschen(tastatur, benutzerListe);
	        } else if (auswahl == 4) {
	        	herunterfahren (benutzerListe);
	        	return;
	        }
	        
            
        }


    }
    
    public static  void anmelden(Scanner tastatur, BenutzerListe benutzerListe) {
    	String name;
    	String passwort;
    	
    	
    	int versuche = 0;
    	
    	while (versuche < 3) {
    		
    	  	System.out.print("Bitte geben Sie Ihren Benutzernamen ein: ");
    	  	name = tastatur.next();
    		
    	  	System.out.print("Bitte geben Sie Ihr Passwort ein: ");
    	  	passwort = tastatur.next();
    	  	
    	  	Benutzer benutzer = benutzerListe.sucheBenutzer(name);
    	  	
  	  	
    	  	
    	  	if (benutzer == null || !Arrays.equals(benutzer.getPasswort(), Crypto.encrypt(passwort.toCharArray()))) {
    	  		
    	  		
    	  		
    	  		System.out.println(!benutzer.getPasswort().equals(Crypto.encrypt(passwort.toCharArray())));
    	  		//System.out.println(benutzer.getPasswort());
    	  		System.out.println(Crypto.encrypt(passwort.toCharArray()));
    	  		    	  		
    	  		
    	  		//System.out.println("Falscher Name oder falsches Passwort.");
    	  		
    	  		name = null;
    	  		passwort = null;
    	  		versuche++;  	  		
    	  	} else {
    	  		System.out.print("Sie sind angemeldet!\n");
    	  		benutzer.setanmeldestatus("online");
    	  		Date date = new Date();
    	  		SimpleDateFormat formatter = new SimpleDateFormat(" dd MMMM yyyy");
    	  		benutzer.setletzterLogin(formatter.format(date));
      	  		return;
    	  	}
    	      	  	
    	}
    	System.out.println("\nAnmeldung fehlgeschlagen.\nSie werden abgemeldet!");
    	
    }
    

	public static void registrieren(Scanner tastatur, BenutzerListe benutzerliste) {
		String name;
		String passwort;
		String passwortBestätigung;
		boolean nutzerBereitsVorhanden = false;
		
		do {
			System.out.print("Bitte geben Sie Ihren gewünschen Nutzernamen ein: ");
			name = tastatur.next();
			
			if (benutzerliste.sucheBenutzer(name) != null) {
				System.out.print("Benutzername bereits vorhanden.");
				nutzerBereitsVorhanden = true;
			} else {
				boolean passwörterSindUngleich = true;
				nutzerBereitsVorhanden = false;		
				
				do {
					System.out.print("Bitte geben Sie Ihr gewünschtes Passwort ein.\n");
					passwort = tastatur.next();
					
					System.out.print("Bitte bestätigen Sie Ihr Passwort: ");
					passwortBestätigung = tastatur.next();
					
					if (passwort.equals(passwortBestätigung)) {
						benutzerliste.insert(new Benutzer(name, Crypto.encrypt(passwort.toCharArray())));
						System.out.print("Sie wurden erfolgreich registriert, " + name + ".");
						passwörterSindUngleich = false;
					} else {
						System.out.println("Die Passwörter stimmen nicht überein.");
					}
					
				} while (passwörterSindUngleich);
	
			}
		} while (nutzerBereitsVorhanden);
		
	}
	
	public static void loeschen (Scanner tastatur, BenutzerListe benutzerliste) {
		
		System.out.println("Geben Sie bitte den Benutzer zum löschen ein!");
		String name = tastatur.next();
		benutzerliste.delete(name);
		
		System.out.println("-test->\n" + benutzerliste.select() + "<-test-\n");
		
	}
	
	public static void herunterfahren (BenutzerListe benutzerliste) throws FileNotFoundException, IOException {
		
		ObjectOutputStream benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerliste);
        benutzerdatei1.close();
    	
        System.out.println(benutzerliste.select());
	}
	
	
}
class BenutzerListe implements Serializable {
    	private Benutzer first;
    
    	public BenutzerListe(){
        first =  null;
        
    }
    
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
    	// den Nachfolger auf null:
	    b.setNext(null);
	    Benutzer h = first;
	    
	    if(first == null){
	        first =  b;
	    } else {
	    	while (h.getNext() != null ) {
	    	h = h.getNext();	
	    	}
	    	h.setNext(b);
	    	
	    }
	}
    
    public String select(){
	    String s = "";
	    Benutzer b = first;
	    while(b != null){
	        s += b.toString() + '\n';
	        b = b.getNext();
	    }
	    return s;
	}
    
	public String select(String name){
	    Benutzer b = first;
	    while(b != null){
	        if(b.hasName(name)){
	            return b.toString();
	        }
	        b = b.getNext();
	    }
	    return "";
	}

	public boolean delete(String name){
    
		Benutzer h = first;
		
		if (sucheBenutzer(name) == null) {
			return false;
		}
		
		
		if (h == sucheBenutzer(name)) {
			first = h.getNext();
		} else 	
		{
			while (h.getNext() != sucheBenutzer(name))
			{
				h = h.getNext();
			}
			
			h.setNext(h.getNext().getNext());		
			
		}
        return true;
    }
    
    public Benutzer sucheBenutzer(String name) {
    	 Benutzer b = first;
         while(b != null){
             if(b.hasName(name)){
                 return b;
             }
             b = b.getNext();
         }
         return null;   
    }
}

class Benutzer implements Serializable {
    private String name;
    private char[] passwort;
    private String anmeldestatus;
    private String letzterLogin;
    
    private Benutzer next;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    	}
    
    public char[] getPasswort() {
    	return passwort;
    }
    
    public String getanmeldestatus() {
    	return anmeldestatus;
    }
    
    public void setanmeldestatus(String status) {
    	anmeldestatus = status;
    }
    
    public String getletzterLogin() {
    	return letzterLogin;
    }
     public void setletzterLogin(String login) {
    	 letzterLogin = login;
     }
}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
}

